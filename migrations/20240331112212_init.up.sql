-- Add up migration script here
begin;
create table if not exists ad_types(
    id serial primary key,
    name varchar(42) not null,
    description varchar(255) not null
);

create table if not exists ads(
    time timestamptz not null default now(),
    ad_type_id integer not null,
    constraint fk_ad_type foreign key (ad_type_id) references ad_types(id) on delete cascade
);
commit