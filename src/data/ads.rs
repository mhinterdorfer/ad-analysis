use anyhow::bail;
use serde::{Deserialize, Serialize};
use sqlx::{FromRow, Pool, Postgres};

#[derive(Clone, FromRow, Debug, Serialize, Deserialize, PartialEq)]
pub struct AdType {
    pub id: i32,
    pub name: String,
    pub description: String,
}

impl AdType {
    pub async fn find_all(executor: &Pool<Postgres>) -> anyhow::Result<Vec<AdType>> {
        let query_result = sqlx::query_as!(AdType, r#"select * from ad_types"#)
            .fetch_all(executor)
            .await;
        match query_result {
            Ok(ad_types) => Ok(ad_types),
            Err(error) => bail!("Error returned from Database"),
        }
    }
    pub async fn insert_occurrence(executor: &Pool<Postgres>, id: i32) -> anyhow::Result<()> {
        let query_result = sqlx::query!(r#"insert into ads (ad_type_id) values($1)"#, id)
            .execute(executor)
            .await;
        match query_result {
            Ok(_) => Ok(()),
            Err(error) => bail!("Error returned from Database"),
        }
    }
}
