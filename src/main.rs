use std::env;

use actix_web::{App, HttpServer, web};
use actix_web::middleware::Logger;
use actix_web::web::Data;
use handlebars::{DirectorySourceOptions, Handlebars};
use log::debug;
use sqlx::postgres::PgPoolOptions;

use crate::net::endpoints::config;

mod data;
mod net;
mod service;

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "info,actix_web=info");
    }

    // initialize dotenvy environment
    let database_url = env::var("DATABASE_URL").expect("Database url must be set");
    let connection_pool = PgPoolOptions::new()
        .max_connections(5)
        .connect(&database_url)
        .await?;

    env_logger::init_from_env(env_logger::Env::new());

    let mut handle_bars_registry = Handlebars::new();
    handle_bars_registry
        .register_templates_directory(
            "./src/static",
            DirectorySourceOptions {
                tpl_extension: ".html".to_owned(),
                hidden: false,
                temporary: false,
            },
        )
        .unwrap();
    let handlebars_ref = web::Data::new(handle_bars_registry);

    let server = HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(handlebars_ref.clone())
            .app_data(Data::new(connection_pool.clone()))
            .service(web::scope("/ads").configure(config))
    })
    .bind(("0.0.0.0", 8080))?
    .run();
    debug!(
        "Server started successfully. Listening on port {} ...",
        8080
    );
    server.await?;
    Ok(())
}
