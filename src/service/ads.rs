use sqlx::{Pool, Postgres};

use crate::data::ads::AdType;

pub async fn get_all_ad_types(db_pool: &Pool<Postgres>) -> anyhow::Result<Vec<AdType>> {
    AdType::find_all(db_pool).await
}
pub async fn log_occurrence(db_pool: &Pool<Postgres>, id: i32) -> anyhow::Result<()> {
    AdType::insert_occurrence(db_pool, id).await
}
