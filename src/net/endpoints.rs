use actix_web::{get, HttpResponse, HttpResponseBuilder, post, Responder, web};
use actix_web::http::StatusCode;
use actix_web::web::{Data, Path};
use handlebars::{Handlebars, to_json};
use serde_json::Map;
use sqlx::{Pool, Postgres};

use crate::service::ads::{get_all_ad_types, log_occurrence};

#[get("")]
async fn index(
    db_pool: Data<Pool<Postgres>>,
    handlebars: web::Data<Handlebars<'_>>,
) -> impl Responder {
    let ad_types = get_all_ad_types(&db_pool).await;
    match ad_types {
        Ok(ad_types) => {
            let mut data = Map::new();
            data.insert("ad_types".to_string(), to_json(&ad_types));
            let body = handlebars.render("index", &data);
            match body {
                Ok(body) => HttpResponse::Ok().body(body),
                Err(error) => {
                    HttpResponseBuilder::new(StatusCode::NO_CONTENT).body(error.to_string())
                }
            }
        }
        Err(error) => {
            HttpResponseBuilder::new(StatusCode::INTERNAL_SERVER_ERROR).body(error.to_string())
        }
    }
}
#[post("{id}")]
async fn log_ad(db_pool: Data<Pool<Postgres>>, path: Path<i32>) -> impl Responder {
    let id = path.into_inner();
    match log_occurrence(&db_pool, id).await {
        Ok(_) => HttpResponse::Ok().finish(),
        Err(error) => {
            HttpResponseBuilder::new(StatusCode::INTERNAL_SERVER_ERROR).body(error.to_string())
        }
    }
}

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(index);
    cfg.service(log_ad);
}
